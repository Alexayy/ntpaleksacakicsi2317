#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Seminarski rad - program za predmet Napredne tehnike programiranja sa zadatom temom:
 *  Implementacija jednostruko spregnute liste sa entitetom "Član biblioteke"
 *
 * Obrazovna ustanova: Univerzitet u Novom Sadu
 *  Tehnički fakultet "Mihajlo Pupin", Đure Đakovića BB, Zrenjanin, Srbija
 *
 * Predmetni profesor: Željko Stojanov
 * Predmetni asistent: Predrag Pecev
 *
 * @author Aleksa Cakić SI 23/17
 * aleksa.cakic@gmail.com
 */

/**
 * Structure that holds information about { @struct LibraryMember }.
 * Acts as a object-oriented term "class", and function { @createNewMember } acts as its constructor.
 */
typedef struct LibraryMember {
    char *Name;
    char *LastName;
    int idNumber;
    double personalNumber;
    char *dateOfBirth;

    char *title;
    char *writer;
    int bookID;
} LibraryMember;

/**
 * "Constructor" for structure ("class") @struct LibraryMember.
 *
 * @param name takes the name of the member
 * @param lastName takes the last name of the registered memeber
 * @param idNumber takes the id number from the users ID
 * @param personalNumber given by the library
 * @param dateOfBirth self explanatory
 * @param title title of the book
 * @param writer a person that wrote the book that member took
 * @param bookID book identification number assigned by library
 * @return member
 */

LibraryMember
createNewMember(char *name, char *lastName, int idNumber, double personalNumber, char *dateOfBirth, char *title,
                char *writer, int bookID) {

    LibraryMember member;
    member.Name = name;
    member.LastName = lastName;
    member.idNumber = idNumber;
    member.personalNumber = personalNumber;
    member.dateOfBirth = dateOfBirth;
    member.title = title;
    member.writer = writer;
    member.bookID = bookID;

    return member;
}

/**
 * Node structures that contains @struct LibraryMember structure as a data parameter
 * and { @param @struct } Node *next as a reference to the next Node in the list.
 *
 */
typedef struct Node {
    LibraryMember member;
    struct Node *next;
} Node;

Node *head = NULL; // initialization
int listSize = 0; // initialization

/**
 * Function that assigns @struct createNode Node it's dedicated spot at memory and creates
 * new Node to be used in other functions.
 *
 * @param member data type that Node structure holds
 * @return newly created node
 *
 */
Node *createNode(LibraryMember member) {
    Node *node = (Node *) malloc(sizeof(Node));
    node->member = member;
    node->next = NULL;

    return node;
}

/**
 * Functions that changes position of @param member in the list by changing its index.
 *
 * @param member data type that Node structure holds
 * @param index position of the member in the list
 *
 */
void edit(LibraryMember member, int index) {
    int i;
    Node *temp = head;

    for (i = 1; i < index; i++) {
        temp = temp->next;
    }

    temp->member = member;
}

/**
 * Function that deletes member of the library on requested index.
 * Takes the index, traverses trough the list and deletes the Node on that position.
 *
 * @param index
 *
 */
void delete(int index) {
    Node *temp = head;
    Node *deleteTemp;
    int i = 0;

    if (listSize <= 0)
        printf("List is empty!\n");
    else if (index == 0) {
        head = head->next;
        free(temp);
        listSize--;
    } else if (index < 0 && index > listSize - 1) {
        printf("Index out bounds.");
        int n;
        delete(scanf("%d", &n));
    } else {

        for (i = 1; i < index; i++) {
            temp = temp->next;
        }

        deleteTemp = temp->next;
        temp->next = deleteTemp->next;
        free(deleteTemp);
        listSize--;
    }
}

/**
 * Function that searches list of members by First Name criteria { @var name }, and prints results if it finds.
 *
 * @param keyword String that is used to find the member by its first name
 *
 */
void searchName(char *keyword) {
    Node *temp = head;
    int nodeFound = 0;
    if (listSize == 0)
        printf("List is empty\n");
    else {
        while (temp != NULL) {
            if (strcmp(temp->member.Name, keyword) == 0) {
                printf("Name: %s\nLast Name: %s\nID Card Number: %d\nPersonal number: %.0lf\nDate of birth: %s\nBook title: %s\nBook Writer: %s\nBook number ID: %d\n",
                       temp->member.Name, temp->member.LastName, temp->member.idNumber, temp->member.personalNumber,
                       temp->member.dateOfBirth,
                       temp->member.title, temp->member.writer, temp->member.bookID);
                nodeFound = 1;

            }

            break;
        }
        if (nodeFound == 0) {
            printf("Member with name: %s not found!\n", keyword);
        }
    }
}

/**
 * Function that searches list of members by Last Name { @var LastName } criteria, and prints results if it finds.
 *
 * @param keyword String that is used to find the member by its last name
 *
 */
void searchLastName(char *keyword) {
    Node *temp = head;
    int nodeFound = 0;

    if (listSize == 0)
        printf("List is empty\n");
    else {
        while (temp != NULL) {

            if (strcmp(temp->member.LastName, keyword) == 0) {
                printf("Name: %s\nLast Name: %s\nID Card Number: %d\nPersonal number: %.0lf\nDate of birth: %s\nBook title: %s\nBook Writer: %s\nBook number ID: %d\n",
                       temp->member.Name, temp->member.LastName, temp->member.idNumber, temp->member.personalNumber,
                       temp->member.dateOfBirth,
                       temp->member.title, temp->member.writer, temp->member.bookID);
                nodeFound = 1;
            }

            break;
        }

        if (nodeFound == 0) {
            printf("Member with last name: %s not found!\n", keyword);
        }
    }
}

/**
 * Function that prints all members that are added to the list.
 *
 * @note this function must me called after all desired actions were performed
 * order to avoid confusion in program.
 */
void printList() {
    Node *temp = head;

    while (temp != NULL) {
        printf("\n+ =============================== +\n");

        printf("\nName: %s\nLast Name: %s\nID Card Number: %d\nPersonal number: %.0lf\nDate of birth: %s\nBook title: %s\nBook Writer: %s\nBook number ID: %d\n",
               temp->member.Name, temp->member.LastName, temp->member.idNumber, temp->member.personalNumber,
               temp->member.dateOfBirth,
               temp->member.title, temp->member.writer, temp->member.bookID);
        temp = temp->next;
    }

    printf("\n+ =============================== +\n");
}

/**
 * A function that appends member to the last position in the list.
 *
 * @param member new Node that will be appended to the list
 * @return added Node
 *
 */
Node *addLast(LibraryMember member) {
    Node *temp = head;

    if (head == NULL)
        head = createNode(member);
    else {

        while (temp->next != NULL) {
            temp = temp->next;
        }

        temp->next = createNode(member);
    }

    listSize++;
    return temp;
}

/**
 * A function the prepends member to the first position in the list.
 *
 * @param member new Node that will be prepended to the list
 * @return added Node
 *
 */
Node *addFirst(LibraryMember member) {
    Node *temp = createNode(member);

    if (temp == NULL)
        head = temp;
    else {
        temp->next = head;
        head = temp;
    }

    listSize++;
    return temp;
}

/**
 * A function that puts a new Node on index passed to this function.
 *
 * @param member new Node that will be added to the list on desired position
 * @param index position
 * @return added Node
 *
 */
Node *addOnPosition(LibraryMember member, int index) {
    int i;
    Node *temp = head;
    Node *input = createNode(member);

    if (listSize == 0)
        addFirst(member);
    if (head == NULL)
        head = input;
    else {
        for (i = 1; i < index; i++) {
            temp = temp->next;
        }

        input->next = temp->next;
        temp->next = input;
    }

    listSize++;
    return input;
}

/**
 * A function that returns the current size of the list.
 * @return listSize number of members in the list
 */
int size() {
    Node *temp = head;
    int size = 0;

    while (temp != NULL) {
        temp = temp->next;
        size++;
    }

    printf("Current number of memebers: %d", size);
    return size;
}

int main() {

    LibraryMember member1 = createNewMember("Aleksa", "Cakic", 10000046, 5456659894, "02.06.1996.",
                                            "Whisperer In the Dark", "H. P. Lovecraft", 666);
    LibraryMember member2 = createNewMember("Snezana", "Mijin", 54711564, 8492165519, "18.12.1995.",
                                            "Norwegian Woods", "Haruki Murakami", 1337);
    LibraryMember member3 = createNewMember("Katarina", "Prentic", 568876696, 944161157, "20.07.1997.",
                                            "Les Miserables", "Victor Hugo", 166);
    LibraryMember member4 = createNewMember("Andjela", "Laudanovic", 54771254, 7494986546, "18.12.1995.",
                                            "Mortal Instruments: City of Bones", "Cassandra Clare", 999);
    LibraryMember member5 = createNewMember("Bogdana", "Manojlovic", 85555777, 1454146417, "17.10.1995",
                                            "Mistborn", "Brandon Sanderson", 1000);
    LibraryMember member6 = createNewMember("Ruzica", "Slijepcevic", 1803995, 547812936333, "18.03.1995.",
                                            "Руслан и Людмила", "Пушкин, Александр Сергеевич", 1010);

    // --------------------------------------------------------------------------------------------------- //

    char *name, keyword;
    int a, b;

    while (a != 0) {
        printf("\n1) Append new member.\n2) Prepend new member.\n3) Add a member to a new position.\n4) Edit member.\n5) Delete member.\n6) Search by criteria\n7) Print list.\n8) Size of the list.\n0) Exit\n");

        while (scanf("%d", &a) != 1) {
            printf("Enter numbers from 0 to 8 to use a function.\n");
        }

        switch (a) {
            case 1:
                printf("\n1) Add Aleksa\n2) Add Snezi\n3) Add Keti\n4) Add Andjela\n5) Add Bogdana\n6) Add Ruska\n");
                scanf("%d", &b);

                switch (b) {
                    case 1:
                        addLast(member1);
                        break;
                    case 2:
                        addLast(member2);
                        break;
                    case 3:
                        addLast(member3);
                        break;
                    case 4:
                        addLast(member4);
                        break;
                    case 5:
                        addLast(member5);
                        break;
                    case 6:
                        addLast(member6);
                        break;
                    default:
                        printf("\nError: member not found.\n");
                        break;
                }

                break;
            case 2:
                printf("\n1) Add Aleksa\n2) Add Snezi\n3) Add Keti\n4) Add Andjela\n5) Add Bogdana\n6) Add Ruska\n");
                scanf("%d", &b);
                switch (b) {
                    case 1:
                        addFirst(member1);
                        break;
                    case 2:
                        addFirst(member2);
                        break;
                    case 3:
                        addFirst(member3);
                        break;
                    case 4:
                        addFirst(member4);
                        break;
                    case 5:
                        addFirst(member5);
                        break;
                    case 6:
                        addFirst(member6);
                        break;
                    default:
                        printf("\nError: member not found.\n");
                        break;
                }

                break;
            case 3:
                printf("\n1) Add Aleksa\n2) Add Snezi\n3) Add Keti\n4) Add Andjela\n5) Add Bogdana\n6) Add Ruska\n");
                scanf("%d", &b);
                printf("\nEnter index\n");

                switch (b) {
                    case 1:
                        addOnPosition(member1, scanf("%d", &a));
                        break;
                    case 2:
                        addOnPosition(member2, scanf("%d", &a));
                        break;
                    case 3:
                        addOnPosition(member3, scanf("%d", &a));
                        break;
                    case 4:
                        addOnPosition(member4, scanf("%d", &a));
                        break;
                    case 5:
                        addOnPosition(member5, scanf("%d", &a));
                        break;
                    case 6:
                        addOnPosition(member6, scanf("%d", &a));
                        break;
                    default:
                        printf("\nError: member not found.\n");
                        break;
                }

                break;
            case 4:
                printf("\nEnter index\n");
                printf("\n1) Add Aleksa\n2) Add Snezi\n3) Add Keti\n4) Add Andjela\n5) Add Bogdana\n6) Add Ruska\n");
                scanf("%d", &b);
                printf("\nEnter index\n");

                switch (b) {
                    case 1:
                        edit(member1, scanf("%d", &a));
                        break;
                    case 2:
                        edit(member2, scanf("%d", &a));
                        break;
                    case 3:
                        edit(member3, scanf("%d", &a));
                        break;
                    case 4:
                        edit(member4, scanf("%d", &a));
                        break;
                    case 5:
                        edit(member5, scanf("%d", &a));
                        break;
                    case 6:
                        edit(member6, scanf("%d", &a));
                        break;
                    default:
                        printf("\nError: member not found.\n");
                        break;
                }

                break;
            case 5:
                printf("\nEnter index\n");
                delete(scanf("%d", &a));
                break;
            case 6:
                printf("\nSearch option: \n");
                int search;
                printf("\n1) Name\n2) Last Name\n");
                scanf("%d", &search);

                switch (search) {
                    case 1:
                        printf("Search name: \n");
                        scanf("%s", &keyword);
                        searchName(&keyword);
                        break;
                    case 2:
                        printf("Search last name: \n");
                        scanf("%s", &keyword);
                        searchLastName(&keyword);
                        break;
                    default:
                        main();
                        break;
                }

                break;
            case 7:
                printList();
                break;
            case 8:
                size();
                break;
            default:
                if (a != 0)
                    printf("\nError, try again.\n");
                break;
        }
    }
}







